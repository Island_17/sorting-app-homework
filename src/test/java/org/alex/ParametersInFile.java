package org.alex;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

/**
 * A class with parameterized test of the arrays sorting method
 * Parameters are taken from the csv-file
 */

@RunWith(Parameterized.class)
public class ParametersInFile {

    SortingApp app = new SortingApp();
    private String[] numbersForSorting;
    private String sortedNumbers;

    public ParametersInFile(String[] line, String expected){
        this.numbersForSorting=line;
        this.sortedNumbers=expected;
    }
    /**
     * Method that create parameters for test method from the file data.txt
     * with arrays that meets requirements to be sorted
     * @return list of parameters for test method and expected result
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        List<Object[]> listOfParams=new ArrayList<>();
        File file=new File("src/test/data/data.txt");
        try {
            Scanner scanner=new Scanner(file);
            while (scanner.hasNextLine()){
                String stringInFile=scanner.nextLine();
                String[] arrayString=stringInFile.split(",");
                String[] arrayToSort=arrayString[0].split(" ");
                listOfParams.add(new Object[]{arrayToSort,arrayString[1]});
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        }
        return listOfParams;
    }
    /**
     * Method that tested method that sorts arrays
     */
    @Test
    public void testRegularCase(){
        String consol=null;
        PrintStream originalOut=System.out;
        try {
            ByteArrayOutputStream outputStream=new ByteArrayOutputStream(100);
            PrintStream capture=new PrintStream(outputStream);
            System.setOut(capture);
            app.sortingArgs(numbersForSorting);
            capture.flush();
            consol=outputStream.toString();
            System.setOut(originalOut);
        } catch (Exception e){
            e.printStackTrace();
        }
        assertEquals(sortedNumbers,consol);
    }
}
