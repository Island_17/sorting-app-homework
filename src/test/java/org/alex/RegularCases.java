package org.alex;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * A class with parameterized test of the arrays sorting method
 */


@RunWith(Parameterized.class)
public class RegularCases {

    SortingApp app = new SortingApp();
    private String[] numbersForSorting;
    private String sortedNumbers;

    public RegularCases(String[] line, String expected){
        this.numbersForSorting=line;
        this.sortedNumbers=expected;
    }

    /**
     * Method that create parameters for test method with arrays that meets requirements to be sorted
     * @return list of parameters for test method and expected result
     */
    @Parameterized.Parameters
    public static Collection<Object[]> data(){
        List<Object[]> listOfParams=new ArrayList<>();
        listOfParams.add(new Object[]{new String[]{"9", "8", "7", "6", "5", "4", "3", "2", "1", "0"},"0 1 2 3 4 5 6 7 8 9"});
        listOfParams.add(new Object[]{new String[]{"500", "400", "300", "200", "100"},"100 200 300 400 500"});
        listOfParams.add(new Object[]{new String[]{"-55", "44", "-33", "22", "-11"},"-55 -33 -11 22 44"});
        listOfParams.add(new Object[]{new String[]{"110"},"110"});
        return listOfParams;
    }

    /**
     * Method that tested method that sorts arrays
     */
    @Test
    public void testRegularCase(){
        String consol=null;
        PrintStream originalOut=System.out;
        try {
            ByteArrayOutputStream outputStream=new ByteArrayOutputStream(100);
            PrintStream capture=new PrintStream(outputStream);
            System.setOut(capture);
            app.sortingArgs(numbersForSorting);
            capture.flush();
            consol=outputStream.toString();
            System.setOut(originalOut);
        } catch (Exception e){
            e.printStackTrace();
        }
        assertEquals(sortedNumbers,consol);
    }

}
