package org.alex;

import org.junit.Test;
import static org.junit.Assert.assertEquals;


/**
 * A class with tests for corner cases of array representation
 */

public class BadArrayCases {

    SortingApp app = new SortingApp();

    /**
     * Test of null case array=null
     */
    @Test
    public void testNullCase() {
        try {
            app.sortingArgs(null);
        } catch (IllegalArgumentException e){
            assertEquals("Nothing to sort!!!", e.getMessage());
        }
    }

    /**
     * Test of empty case when array.length=0
     */
    @Test(expected = IllegalArgumentException.class)
    public void testEmptyCase() {
        String[] array = new String[0];
        app.sortingArgs(array);
    }

    /**
     ** Test of case when array.length>10
     */
    @Test
    public void testLengthOfArray() {
        String[] array = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        try {
            app.sortingArgs(array);
        } catch (IllegalArgumentException e){
            assertEquals("Array length more than 10!!!",e.getMessage());
        }
    }

    /**
     ** Test of case when array element is not an integer value
     */
    @Test(expected = NumberFormatException.class)
    public void testArrayWithIlligalElement() {
        String[] array = new String[]{"0", "1", "a", "3", "4", "5", "6", "7", "8"};
        app.sortingArgs(array);
    }

}
