/**
 * Maven-based project - Sorting App.
 * </p>
 * It is a small Java application that takes up to ten command-line arguments as integer values,
 * sorts them in the ascending order, and then prints them into standard output.
 * </p>
 * @author Oleksii Kriachko
 * @version 1.0
 *
 */

package org.alex;