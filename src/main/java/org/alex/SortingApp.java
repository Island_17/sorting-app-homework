/**
 * @author Oleksii Kriachko
 * @version 1.0
 */
package org.alex;

import java.util.*;

/**
 * A class with a method
 * for sorting a numeric array represented as an array of strings

 */
public class SortingApp {
    /**
     *Method for sorting in ascending order
     * an array of numbers represented as an array of strings
     * @param args array to sort
     * @throws IllegalArgumentException if the incoming array
     * or some of its element does not meet the conditions for the array to be sorted
     */

    public void sortingArgs(String[] args) throws IllegalArgumentException{
        /**
         * List for Numeric data
         */
        List<Integer> list = new ArrayList<>();

        /**
         * Checking the incoming array for null, emptiness
         * and compliance with the size
         */
        if (args == null || args.length == 0) {
            throw new IllegalArgumentException("Nothing to sort!!!");
        }
        if(args.length>10) throw new IllegalArgumentException("Array length more than 10!!!");

        /**
         *Converting string representation array data
         * into number data and storing it in list
         */
        for (String arg : args) {
            list.add(Integer.parseInt(arg));
        }
        /**
         * Sorting data in list
         */

        list.sort(Comparator.naturalOrder());

        //* Print sorted list
        StringJoiner str=new StringJoiner(" ");
        list.forEach(e->str.add(Integer.toString(e)));
        System.out.print(str);

    }

    /**
     * Start point of application,
     * passing to a method for sorting an array of numbers in string representation
     * @param args - command-line arguments as integer values to sort
     */
    public static void main(String[] args) {
        SortingApp app=new SortingApp();
        app.sortingArgs(args);
    }
}
